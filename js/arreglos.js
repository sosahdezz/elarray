let miArreglo = [];
let otroArreglo = [2, 4, 6, 12, 20];

// Acceso a un elemento de array
console.log(otroArreglo[0]);
console.log("El valor es " + otroArreglo[3]);

//Longitud array
console.log(otroArreglo.length);

//Mostrar números aleatorios
let aleatorio = (Math.random() * 100).toFixed(0);
console.log("El número es " + aleatorio);

function pro(otroArreglo) {
    let acumular = 0;
    let promedio = 0;
    for (let i = 0; i < otroArreglo.length; ++i) {
        otroArreglo[i] = (Math.random() * 100).toFixed(0);
        acumular = + otroArreglo[i];
    }
    promedio = acumular / otroArreglo.length;
    return promedio.toFixed(1);

}

let res = pro(otroArreglo);
console.log(res);

//Tarea css y botones
function llenar(otroArreglo) {
    const cantidad = document.getElementById('txtCantidad').value;
    otroArreglo.length = 0; 
    for (let i = 0; i < cantidad; ++i) {
        otroArreglo.push((Math.random() * 100).toFixed(0)); 
    }
}


function mostrar() {
    llenar(otroArreglo);
    numPares(otroArreglo);
    numImpares(otroArreglo);
    const lista = document.getElementById('lista');

    lista.innerHTML = '';
    otroArreglo.forEach((valor, index) => {
        lista.options.add(new Option(valor, index));
    });

    const simetrico = realSimetrico(otroArreglo);
    document.getElementById('txtSimetrico').value = simetrico ? 'Si es Simetrico :)' : 'No es Simetrico :('; 
}

function numPares(otroArreglo) {
    const txtPares = document.getElementById('txtPares');
    let pares = 0;
    for (let i = 0; i < otroArreglo.length; ++i) {
        if (otroArreglo[i] % 2 == 0) ++pares;
    }
    txtPares.value = pares;
}

function numImpares(otroArreglo) {
    const txtImpares = document.getElementById('txtImpares');
    let impares = 0; 
    for (let i = 0; i < otroArreglo.length; ++i) {
        if (otroArreglo[i] % 2 !== 0) ++impares; 
    }
    txtImpares.value = impares; 
}

function borrarLista() {
    const lista = document.getElementById('lista');

    while (lista.firstChild) {
        lista.removeChild(lista.firstChild);
    }
    document.getElementById('txtCantidad').value = "";
    document.getElementById('txtPares').value = "";
    document.getElementById('txtImpares').value = "";
    document.getElementById('txtSimetrico').value = "";
}

function realSimetrico(arrayz) {
    let pares = 0;
    let impares = 0;
    
    for (let i = 0; i < arrayz.length; i++) {
        if (arrayz[i] % 2 === 0) {
            pares++;
        } else {
            impares++;
        }
    }
    
    return pares === impares;
}
